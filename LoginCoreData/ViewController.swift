//
//  ViewController.swift
//  LoginCoreData
//
//  Created by burak on 27.02.2020.
//  Copyright © 2020 burak. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var emailArr = [String]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return emailArr.count
    }


 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        cell.textLabel?.text = emailArr[indexPath.row]
        
        return cell
    }

    
    @IBAction func saveUser(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
        
        newUser.setValue(emailTextField.text, forKey: "email")
        newUser.setValue(passwordTextField.text, forKey: "password")
        
        do{
            try context.save()
            print("success")
        }
        catch{
            print(error)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    if let email = result.value(forKey: "email") as? String{
                        self.emailArr.append(email)
                        print("registered email")
                    }
                    if (result.value(forKey: "password") as? String) != nil{
                        print("registered password")
                    }
                }
            }
            print(results)
        }
        catch{
            print(error)
        }
    }


}

